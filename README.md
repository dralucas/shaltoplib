# shaltoplib

This is the ShaltopLib repository. A complete toolbox for reading and analysing simulation results from Shatlop, a shallow water numerical model for geophysical granular flows, such as landslides or avalanches. 



The Read_svsh notebook offers some examples of the usage of the library (`shaltoplib.py`).

